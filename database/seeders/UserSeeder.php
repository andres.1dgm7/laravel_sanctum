<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = new User();
        $user->name = 'vins';
        $user->email = 'vins@gmail.com';
        $user->password = Hash::make('123456');
        $user->fecha = date('Y-m-d');
        $user->save();

        User::factory(20000)->create();
    }
}
