<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona_natural extends Model
{
    use HasFactory;
    protected $table = 'persona_natural';
    protected $primaryKey = 'id_persona';
    public $timestamps = false;
    
}
