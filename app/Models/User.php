<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

     protected $table = "users";
    protected $primaryKey = 'id';
    // protected $fillable = [
    //     'name',
    //     'email',
    //     'password',
    // ];

    public $timestamps = false;
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    // protected $hidden = [
    //     'password',
    //     'remember_token',
    // ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];
    public function estado()
    {
        return $this->belongsTo(Estados::class, 'id_estado', 'id_estado');
    }
    public function municipio()
    {
        return $this->belongsTo(Municipios::class, 'id_municipio', 'id_municipio');
    }
    public function parroquia()
    {
        return $this->belongsTo(Parroquias::class, 'id_parroquia', 'id_parroquia');
    }
    public function perfil()
    {
        return $this->belongsTo(Perfiles::class, 'id_perfil', 'id_perfil');
    }
    public function email()
    {
        return $this->belongsTo(PasswordReset::class, 'email', 'email');
    }


}
