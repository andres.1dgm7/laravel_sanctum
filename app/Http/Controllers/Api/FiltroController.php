<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Estados;
use App\Models\Municipios;
use App\Models\Parroquias;

class FiltroController extends Controller
{
    //
    public function estados(){
        $estados = Estados::all();
        return response()->json([
            'estados'=>$estados
        ], 200);
    }

    public function municipio($id){
        $municipio = Municipios::where('id_estado','=',$id)->get();
         return response()->json([
            'municipio'=>$municipio
        ], 200);
    }

     public function parroquias($id){
        $parroquia = Parroquias::where('id_municipio','=',$id)->get();
         return response()->json([
            'parroquia'=>$parroquia
        ], 200);
    }
}
