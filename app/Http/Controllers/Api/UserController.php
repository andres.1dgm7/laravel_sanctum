<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
// use Symfony\Component\HttpFoundation\Cookie;
use Cookie;
use App\Models\User;
use App\Models\Persona_natural;
use App\Models\Organizacion;
use App\Models\PasswordReset;
use App\Mail\RecuperarPassMailable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use DataTables;

use App\Mail\ContactanosMailable;
use Exception;
use Illuminate\Support\Facades\Mail;

//use Illuminate\Auth\Events\Registered;
//use Illuminate\Notifications\Notification;
use PDF;

class UserController extends Controller
{
    //
    public function check(){  
        return Auth::user()->id_perfil;
    }

    public function register(Request $request){

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->fecha = date('Y-m-d');
        $user->codigo = bin2hex(random_bytes(10));
        $user->verificado = false;
        $user->id_estado = $request->id_estado;
        $user->id_municipio = $request->id_municipio;
        $user->id_parroquia = $request->id_parroquia;
        $user->id_perfil = $request->tipo;
        $user->save();
        $form = "";
        if($request->tipo === "4"){
            $form = new Persona_natural;
        }else if($request->tipo === "5"){
            $form = new Organizacion;
        }
        $form->rif_cedula = $request->rif_cedula;
        $form->razon_social = $request->razon_social;
        $form->telefono = $request->telefono;
        $form->domicilio_fiscal = $request->domicilio_fiscal;
        $form->id_user = $user->id;
        $form->save();
         try{
               Mail::to('carlosvictoraio@gmail.com')->send(new ContactanosMailable($user->id));
            }catch(Exception $e){
                return response()->json([
                    "status" => false,
                    "message" => $e->getMessage()
                ]);
                //eliminar el usuario
        }
        return response()->json([
            "status" => true,
            "message" => "Usuario creado exitosamente, 
            se le enviara un correo a la dirección email que nos proporciono"
        ], 200);
    }

    public function login(Request $request): JsonResponse
    {
        $user = User::where('name', '=', $request->name)->first();
        if(isset($user->id)){
            if($user->verificado == true){
                if(Hash::check($request->password, $user->password)){
                    $token = $user->createToken("token")->plainTextToken;
                    return response()->json([
                        'status' => true,
                        'msj' => 'Usuario Logueado',
                        'token'=> $token
                    ]);
                }else{
                    return response()->json([
                        "status" => false,
                        "msg" => "Clave Invalida",
                    ], Response::HTTP_UNAUTHORIZED);
                }
            }else{
                return response()->json([
                    "status" => false,
                    "msg" => "Usuario no verificado",
                ], Response::HTTP_NOT_ACCEPTABLE);
            }

        }else{
            return response()->json([
                "status" => false,
                "msg" => "Usuario no registrado",
            ], Response::HTTP_UNAUTHORIZED);
        }
    }

    public function user_profile(): JsonResponse
    {
        return response()->json([
            "status" => 1,
            "msg" => "logueado",
            "data" => Auth::user()
        ], Response::HTTP_OK);
    }

    public function logout(): JsonResponse
    {

        Auth::user()->tokens()->delete();
        // $cookie = Cookie::forget('cookie_token');
        return response()->json([
            'status'=>true,
            "msj"=>"Cierre de Sesion"
        ], Response::HTTP_OK);
        // ->withoutCookie($cookie);
    }

    public function usuarios()
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $user = User::leftjoin('estados','estados.id_estado','=','users.id_estado')->
                      leftjoin('municipios','municipios.id_municipio','=','users.id_municipio')->
                      leftjoin('parroquias','parroquias.id_parroquia','=','users.id_parroquia')->
                      leftjoin('perfiles','perfiles.id_perfil','=','users.id_perfil')->get();
        return Datatables::of($user)->make(true);
    }

    public function verificacion(Request $request)
    {
        $user =User::find($request->id);
            if($request->codigo == $user->codigo){
                $user->verificado = true;
                $user->save();
                $token = $user->createToken("token")->plainTextToken;
                return response()->json([
                    'status'=>true,
                    'token' => $token
                ], Response::HTTP_OK);
            }
        
            return response()->json([
                'status'=>false,
            ]);
    }

    public function delete($id)
    {
        $user = User::where('id',$id)->delete();
      return $user;
    }
    public function u_update(Request $request)
    {
        $user = User::find($request->id);
        $user->id_estado = $request->est;
        $user->id_municipio = $request->mun;
        $user->id_parroquia = $request->par;
        $user->save();
        if($user){
            return true;
        }
        return false;
    }

    public function pdf(){

        //$comando = shell_exec("cd ../storage/app/certificados/ && openssl pkcs12 -in prueba2.p12 -nokeys -out prueba2.pem //-password pass:password");
        $comando = shell_exec("cd ../storage/app/certificados/ && openssl pkcs12 -in probando.p12 -out probando.pem -clcerts -nodes -password pass:password");
    
        //PDF::SetPrintHeader(false);
        //PDF::SetPrintFooter(false);
        $certificate = 'file://'.base_path().'/storage/app/certificados/probando.pem';
        $certificate_key = 'file://'.base_path().'/storage/app/certificados/prueba3.pkey';
            // set additional information in the signature
        $info = array(
            'Name' => 'Kaushal Kushwaha',
            'Location' => 'Indore',
            'Reason' => 'Generate Demo PDF',
            'ContactInfo' => '',
        );
        PDF::setSignature($certificate, $certificate,'password', '', 2, $info);
        PDF::SetFont('helvetica', '', 12);
        PDF::SetCreator('Kaushal Kushwaha');
        PDF::SetTitle('new-pdf');
        PDF::SetAuthor('Kaushal');
        PDF::SetSubject('Generated PDF');
        PDF::AddPage();
        $html = '<div>
            <h1>What is Lorem Ipsum?</h1>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            Lorem Ipsum has been the industry`s standard dummy text ever since the 1500s,
            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
            It has survived not only five centuries, but also the leap into electronic typesetting, 
            remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
            sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like 
            Aldus PageMaker including versions of Lorem Ipsum.
        </div>';
        PDF::writeHTML($html, true, false, true, false, '');
        //PDF::Image('kaushalkushwaha.png', 5, 75, 40, 15, 'PNG');
        //PDF::setSignatureAppearance(5, 75, 40, 15);
        PDF::Output('probando.pem.pdf','D');
        PDF::reset();
        echo "PDF Generated Successfully";
    }
    public function pdfd(){
        return response()->download(public_path('probando.pem.pdf'));
    }

    public function user_f(){
        $user = User::where('id',Auth::user()->id)
        ->with('estado','municipio','parroquia','perfil')
        ->get();
        return $user;
    }
    public function change_pass(Request $request){
        if($request->pass1 == $request->pass2){
            $user = User::find(Auth::user()->id);
            $user->password = Hash::make($request->pass1);
            $user->save();
            return response()->json([
                'status'=>true,
                'message' => 'Contraseña actualizada exitosamente!.'
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'message' => 'Las contraseñas suministradas no coinciden!.',
                'status' => false
            ]);
        }
    }
    public function recuperar_pass(Request $request){
        $user = User::where('name',$request->name)->where('email',$request->email)->first();
        if(isset($user->id)){
            date_default_timezone_set('America/Caracas');
            $passreset = new PasswordReset();
            $passreset->id_user = $user->id;
            $passreset->email = $request->email;
            $passreset->created_at = date('Y-m-d H:i:s');
            $passreset->expired_time = date('Y-m-d '.(date('H')+1).':i:s');

            $passreset->token = bin2hex(random_bytes(10));
            $passreset->valido = true;
            $passreset->save();
            try{
            Mail::to('carlosvictoraio@gmail.com')->send(new RecuperarPassMailable($passreset));
            }catch(Exception $e){
                return response()->json([
                    'status'=>false,
                    'message'=>'Ha ocurrido un error al enviar el email'
                ], Response::HTTP_NOT_ACCEPTABLE);
            }
            return response()->json([
                'status'=>true,
                'message'=>'Se ha enviado un correo a su dirección email porfavor ingrese en su correo y verifique'
            ], Response::HTTP_OK);
        }
        return response()->json([
            'status'=>false,
        ], Response::HTTP_UNAUTHORIZED);
    }
    public function ext_pass(Request $request){
        $usereset = PasswordReset::find($request->id);
        if(isset($usereset->id)){
            if($usereset->token == $request->token && $usereset->valido){
                if($request->pass1 == $request->pass2){
                    $usereset->valido = false;
                    $usereset->save();
                    $user = User::find($usereset->id_user);
                    $user->password = Hash::make($request->pass1);
                    $user->save();
                    $token = $user->createToken("token")->plainTextToken;
                    return response()->json([
                        'status'=>true,
                        'token'=>$token,
                        'message' => 'Contraseña actualizada exitosamente. Espere mientras lo redireccionamos'
                    ], Response::HTTP_OK);
                }
            }
        }
       
        return response()->json([
            'status'=>false,
            'message' => 'Ha ocurrido un error, porfavor vuelva ha realizar el proceso'
        ], Response::HTTP_UNAUTHORIZED);

    }
    public function expired_time($token,$id){
        $pass = PasswordReset::find($id);
        if(isset($pass->id)){
            if($pass->token == $token){
                if($pass->valido){
                    date_default_timezone_set('America/Caracas');
                    $f_actual = date('Y-m-d H:i:s');
                    $f_expired = $pass->expired_time ;
                    if($f_actual > $f_expired){
                        return response()->json([
                            'status'=> false,
                            'message' => 'Link Expirado !'
                        ], Response::HTTP_OK);            
                    }
                }else{
                    return response()->json([
                        'status'=> false,
                        'message' => 'Link Usado !'
                    ], Response::HTTP_OK);
                }
            }else{
                return response()->json([
                    'status'=> false,
                    'message' => 'Token Incorrecto !'
                ], Response::HTTP_OK);       
            }
        }
        return response()->json([
            'status'=>true,
            'message' => 'Link Valido !',
            'expired' => $pass->expired_time ,
            'data' => date('Y-m-d H:i:s')
        ], Response::HTTP_OK); 
    }
    public function pruebalogin(Request $request){
        return $request;
    }
}


