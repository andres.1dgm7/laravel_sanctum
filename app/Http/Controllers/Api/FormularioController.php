<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Formulario;
use Illuminate\Support\Facades\Auth;
use App\Models\User;



class FormularioController extends Controller
{




    public function crear_form(Request $request){
        $form = new Formulario();
        $form->id_user = Auth::user()->id;
        $form->color = $request->color;
        $form->deporte = $request->deporte;
        $form->fecha = date('Y-m-d');
        $form->save();

        return response()->json([
            "status" => 1,
            "msj" => "Formulario creado"
        ]);
    }

    public function show_form(){
        $forms = Formulario::where('id_user', '=', Auth::user()->id)->get();
        return response()->json([
            "status" => 1,
            "msj" => "Listados",
            "data" => $forms
        ]);
    }

    public function show_one_form($id){

    }

    public function update_form(Request $request, $id){
        if(Formulario::where(["id_user" => Auth::user()->id, "id"=>$id])->exists()){
            $form = Formulario::find($id);
            $form->color = isset($request->color) ? $request->color : $form->color;
            $form->deporte = isset($request->deporte) ? $request->deporte : $form->deporte;
            $form->save();
            return response()->json([
                "status" => 1,
                "msj" => "Formulario Actualizado"
            ]);
        }else{
            return response()->json([
                "status" => 0,
                "msj" => "Formulario no encontrado"
            ], 404);
        }
    }

    public function delete_form($id){
        if(Formulario::where(["id_user" => Auth::user()->id, "id"=>$id])->exists()){
            $form = Formulario::where(["id_user" => Auth::user()->id, "id"=>$id])->first();
            $form->delete();
            return response()->json([
                "status" => 1,
                "msj" => "Formulario Eliminado"
            ]);
        }else{
            return response()->json([
                "status" => 0,
                "msj" => "Formulario no encontrado"
            ], 404);
        }
    }

    public function upload_documents(Request $request){
        /*
               foreach($request->file as $f){
                   if($request->hasFile('file')){
                       $file = $f;
                       $filename = $file->getClientOriginalName();//nombre original
                       $filename = pathinfo($filename, PATHINFO_FILENAME);// nombre sin la extension
                       $name_File = str_replace(" ", "_", $filename);//nombre sin espacios
                       $extension = $file->getClientOriginalExtension();
                       $picture = date('His').'-' . $name_File . '.' .$extension;
                       $file->move(public_path('Files/'), $picture);
           
                   }else{
                       return response()->json(["mensaje" => "error"]);
                   }
               }
               return response()->json(["mensaje" => "carga exitosa"]);*/
               $ftp_server = "10.16.101.81";
               $conn_id = ftp_connect($ftp_server) or die("No se pudo conectar a $ftp_server"); 
               $path = "/var/www/html";
               ftp_login($conn_id,'usuario','123456');
               ftp_pasv($conn_id,true);
               ftp_chdir($conn_id,$path);
               $dir = ftp_nlist($conn_id,$path);
               //$carpeta = ftp_mkdir($conn_id,'probando8');
               $año = date('Y');
               $mes = date('m');
               $ruta_año = "/var/www/html/$año";
               $ruta_mes = "/var/www/html/$año/$mes";
               $existe_a = false;
               $existe_m = false;
               
               foreach($dir as $f){
                   if($f == $ruta_año){
                       $existe_a = true;
                   }
               }
       
               if($existe_a == false ){
                   ftp_mkdir($conn_id,$año);
                   ftp_chdir($conn_id,$año);
               }else{
                   ftp_chdir($conn_id,$año);
       
               }
       
               //////////
               $dir = ftp_nlist($conn_id,$ruta_año);
               foreach($dir as $f){
                   if($f == $ruta_mes){
                       $existe_m = true;
                   }
               }
               if($existe_m == false ){
                   ftp_mkdir($conn_id,$mes);
                   ftp_chdir($conn_id,$mes);
               }else{
                   ftp_chdir($conn_id,$mes);
               }
       
           foreach($request->file as $f){
                      if($request->hasFile('file')){
                           $file = $f;
                           $filename = $file->getClientOriginalName();//nombre original
                           $filename = pathinfo($filename, PATHINFO_FILENAME);// nombre sin la extension
                           $name_File = str_replace(" ", "_", $filename);//nombre sin espacios
                           $extension = $file->getClientOriginalExtension();
                           $picture = $name_File . '.' .$extension;                     
                           ftp_put($conn_id,$picture,$file,FTP_ASCII);               
                       }else{
                           return response()->json(["mensaje" => "error"]);
                       }
                      
                   }
       
           ftp_quit($conn_id);
           return response()->json(["mensaje" => 'exito']);
       
           }
       
       
       /*
           $ftp_server = "10.16.101.23";
           $archivo = 'prueba.'.$request->file->extension();
           
           $conn_id = ftp_connect($ftp_server) or die("No se pudo conectar a $ftp_server"); 
           ftp_login($conn_id,'usuario','tfv9b17qjch7');
           ftp_pasv($conn_id,true);
           ftp_put($conn_id,$archivo,$request->file,FTP_BINARY);
           ftp_quit($conn_id);
           return response()->json(["mensaje" => $conn_id]);*/
}
