<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\FormularioController;
use App\Http\Controllers\Api\FiltroController;

use Illuminate\Foundation\Auth\EmailVerificationRequest;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('prueba-login', [UserController::class, 'pruebalogin'])->name('prueba-login');



Route::post('register', [UserController::class, 'register'])->name('register');
Route::post('login', [UserController::class, 'login'])->name('login');
Route::post('recuperar_contra', [UserController::class, 'recuperar_pass'])->name('recuperar_contra');
Route::post('ext-pass', [UserController::class, 'ext_pass'])->name('ext-pass');
Route::get('expired/{token}/{id}', [UserController::class, 'expired_time'])->name('expired');

//Filtro
Route::get('estados', [FiltroController::class, 'estados'])->name('estados');
Route::get('municipio/{id}', [FiltroController::class, 'municipio'])->name('municipio');
Route::get('parroquias/{id}', [FiltroController::class, 'parroquias'])->name('parroquias');

Route::group( ['middleware' => ["auth:sanctum"]], function(){
    Route::get('check', [UserController::class, 'check'])->name('check');


    //Rutas
    Route::get('logout', [UserController::class, 'logout'])->name('logout');
    Route::get('user_profile', [UserController::class, 'user_profile'])->name('user_profile');
    Route::get('usuarios', [UserController::class, 'usuarios'])->name('usuarios');
    Route::get('delete/{id}', [UserController::class, 'delete'])->name('delete');
    Route::post('change-pass', [UserController::class, 'change_pass'])->name('change-pass');

    ////////////CRUD//////////////////////////////////////////////////////////////
    Route::post('crear_form', [FormularioController::class, 'crear_form'])->name('crear_form');
    Route::get('show_form', [FormularioController::class, 'show_form'])->name('show_form');
    Route::post('update', [UserController::class, 'u_update'])->name('update');
    Route::get('show_one_form/{id}', [FormularioController::class, 'show_one_form'])->name('show_one_form');

    Route::put('update_form/{id}', [FormularioController::class, 'update_form'])->name('update_form');
    Route::delete('delete_form/{id}', [FormularioController::class, 'delete_form'])->name('delete_form');
});

Route::post('upload_documents', [FormularioController::class, 'upload_documents'])->name('upload_documents');
Route::post('verificacion', [UserController::class, 'verificacion'])->name('verificacion');
Route::get('pdf', [UserController::class, 'pdf'])->name('pdf');
Route::middleware('auth:sanctum')->get('/user_f',[UserController::class,'user_f']);

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/
